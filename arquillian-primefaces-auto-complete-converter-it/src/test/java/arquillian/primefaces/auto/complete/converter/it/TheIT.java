package arquillian.primefaces.auto.complete.converter.it;

import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omnifaces.utils.arquillian.ArquillianPrimeFaces;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author richter
 */
@RunWith(Arquillian.class)
public class TheIT {

    @Deployment(testable = false)
    @SuppressWarnings("SleepWhileInLoop")
    public static Archive<?> createDeployment0() {
        WebArchive retValue = Maven.configureResolver().workOffline().resolve("richtercloud:arquillian-primefaces-auto-complete-converter-web:war:1.0-SNAPSHOT")
                .withoutTransitivity().asSingle(WebArchive.class);
        return retValue;
    }

    @Drone
    private WebDriver browser;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id = "mainForm:autoComplete")
    private WebElement autoComplete;

    @Test
    public void testAll() {
        browser.get(deploymentUrl.toExternalForm()+"/index.xhtml");
        String keys = "The text which contains spaces";
        ArquillianPrimeFaces.setAutoCompleteValue(autoComplete, "some value");
            //@TODO: should share constant "some value"
    }
}
