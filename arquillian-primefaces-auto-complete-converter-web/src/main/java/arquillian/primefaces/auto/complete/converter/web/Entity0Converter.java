package arquillian.primefaces.auto.complete.converter.web;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 *
 * @author richter
 */
//public class Entity0Converter implements Converter<Entity0> {
public class Entity0Converter implements Converter {
    public static final String ONLY_ENTITY0_PROPERTY0 = "some value";

    @Override
    public Entity0 getAsObject(FacesContext context, UIComponent component, String value) {
        if(value == null) {
            return null;
        }
        if(!value.equals(ONLY_ENTITY0_PROPERTY0)) {
            return null;
        }
        return new Entity0(ONLY_ENTITY0_PROPERTY0);
    }

    @Override
//    public String getAsString(FacesContext context, UIComponent component, Entity0 value) {
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value == null) {
            throw new ConverterException();
        }
//        return value.getProperty0();
        return ((Entity0)value).getProperty0();
    }
}
