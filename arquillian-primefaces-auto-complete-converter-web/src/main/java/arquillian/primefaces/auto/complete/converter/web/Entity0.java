package arquillian.primefaces.auto.complete.converter.web;

/**
 *
 * @author richter
 */
public class Entity0 {
    private String property0;

    public Entity0() {
    }

    public Entity0(String property0) {
        this.property0 = property0;
    }

    public String getProperty0() {
        return property0;
    }

    public void setProperty0(String property0) {
        this.property0 = property0;
    }
}
