package arquillian.primefaces.auto.complete.converter.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author richter
 */
@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private static final long serialVersionUID = 1L;
//    private Entity0 value;
    private String value;
//    private Converter<Entity0> entity0Converter = new Entity0Converter();
    private Converter entity0Converter = new Entity0Converter();

    public BackingBean0() {
    }

//    public Entity0 getValue() {
//        return value;
//    }
//
//    public void setValue(Entity0 value) {
//        this.value = value;
//    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public Converter<Entity0> getEntity0Converter() {
//        return entity0Converter;
//    }
//
//    public void setEntity0Converter(Converter<Entity0> entity0Converter) {
//        this.entity0Converter = entity0Converter;
//    }

    public Converter getEntity0Converter() {
        return entity0Converter;
    }

    public void setEntity0Converter(Converter entity0Converter) {
        this.entity0Converter = entity0Converter;
    }

    public List<String> complete(String query) {
        return new ArrayList<>(Arrays.asList(Entity0Converter.ONLY_ENTITY0_PROPERTY0));
    }
}
